# User Guide (external): Open WIN MR Protocols Database

> A guide for external researchers on how to use the Open WIN MR protocols database

> Bug reports or feature requests identified in creating this material should be communicated to the developers as an issue on the [acquisition (protocols) database repo](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db/-/issues).

----

**Contents**
- [Purpose](#purpose)
- [Scope](#scope)
  - [Other resources](#other-resources)
- [Intended Audience](#intended-audience)
- [How To Access MR Protocols](#how-to-access-the-open-win-mr-protocols-database)
- [How To Cite A Protocol](#how-to-cite-a-protocol)
- [Example](#example)
- [How To Create Your Own Protocols Database](#how-to-create-your-own-protocols-database)
- [Contact](#contact)
  - [Feedback](#feedback)


## Purpose

> What is the Open WIN MR Protocols Database?

This doc provides guidance for *external* users. Text will be drafted here and added to the help page of [the database](https://open.win.ox.ac.uk/protocols)




## Scope

> What does this guide cover?

- How to access the database.
- How to search for specific protocols using keywords.
- How to view details of individual protocols.
- How to download protocols.

> What is not covered here?

- How to use a protocol on your scanner.


### Other resources

The Open WIN MR Protocols Database is designed and hosted by the [Wellcome Centre for Integrative Neuroimaging (WIN Centre)](https://www.win.ox.ac.uk/) at the [University of Oxford](https://www.ox.ac.uk/). The database is part of [**Open WIN Tools**](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tools/), a collection of software tools and infrastructure to openly and effectively share research outputs, incl. acquisition protocols, experimental tasks for fMRI, FSL analysis pipelines and data.


## Intended Audience

>  Who is this for? Who is an external user?

<mark> TODO: Who is an external user? </mark>

An *external user* is anyone without Oxford University Single-Sign-On (SSO) credentials or anyone outside WIN (**?**)

Anyone looking for a specific MR protocol -- for informational, reference, teaching, research or any other purposes -- can download open access protocols.
Each deposited protocol contains all information necessary to reproduce that sequence on an appropriately licensed scanner. The aim of this database is to speed up translation across groups and institutions by facilitating open and reproducible research.

*Internal users* can also access non-public protocols. For more information, see our [guide for internal users](user-guide-internal.md).


## How To Access The Open WIN MR Protocols Database

![gif - external](./figures_external/gif_external.gif)

- Go to start page: https://open.win.ox.ac.uk/protocols/
- No login required to download open access protocols
- Enter a keyword or search term in the search box
- Browse list of available protocols
- Select individual protocols to view acquisition details
- Download protocol (includes meta-info and full sequence information)


---

## Example

**1. Search** for all open access (i.e. publicly available) protocols that contain the keyword *T1*

![WIN MR Database - Home](./figures_external/screenshot_mr_db_home.png)

**2.** The search returns a **list of open access protocols** containing *T1*, with additional information on species, project name and date of most recent changes to each entry.

![WIN MR Database - Search](./figures_external/screenshot_mr_db_search.png)

**3. Select** the **UK Biobank v4.0_0001** protocol to view details. The first section contains some general information about the protocol, including a stable URL for quick access, version history and scanning info.

![WIN MR Database - Protocol overview](./figures_external/screenshot_protocol_overview.png)

**4.** The section **Protocol Info** provides some additional meta-information (project name, description, usage guidance) and a summary of the sequences used. Below the summary, detailed information is given for each indivdual sequence, incl. keywords, description and the necessary acquisition time.

![WIN MR Database - Protocol info](./figures_external/screenshot_protocol_info1.png)
![WIN MR Database - Protocol info](./figures_external/screenshot_protocol_info2.png)

**5. Download** protocol: At the top of the page, just below the protocol name is the download button. This will lead to a download link for a zip file that contains the information shown in the protocol overview plus full configuration details for each sequences (not shown online).

![WIN MR Database - Protocol download](./figures_external/screenshot_protocol_download1.png)
![WIN MR Database - Protocol download](./figures_external/screenshot_protocol_download2.png)


Note that the amount of information and detail shown may vary across protocols. For additional information and questions, please refer to information, links and contact information given under "Usage Guidance" in the *Protocol Info* section.


&nbsp;

## How To Cite A Protocol

Please use the DOI associated with each protocol when citing. The preferred format is *Author(s). Year. Protocol name. DOI. URL. (??)*


## How To Create Your Own Protocols Database

The main source code for this database is openly available on [gitlab](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db).

If you reuse the code to build your own database, please cite it as <mark>Open WIN MR Protocols Database. XX. YY. DOI. ??</mark>


## Contact

Information about who to contact about this protocol should be provided in the usage notes (*Protocol Info -> Usage Guidance*). If not and you really need to contact someone, please [get in touch with us](https://open.win.ox.ac.uk/protocols/about).

### Feedback

For feedback on this documentation, please use [gitlab issues](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db/-/issues).

~

# <mark>Notes for us when writing</mark>
Things to add:
- [x] Info for who to contact about this sequence should be written in the usage notes. If it it not and you really need to speak to someone, please [contact us](https://open.win.ox.ac.uk/protocols/about).
- [x] How to cite
- [x] Download only access
- [x] Give link to how to feedback on this documentation (gitlab issues)

# <mark>Open issues</mark>
- [x] Can the downloaded file be named something more meaningful? (e.g. "WIN_open_protocol_\<Name\>")
- [x] What is the preferred format for citing a protocol? E.g. something like *Author(s). Year. Protocol name. DOI. URL. (??)* - as specified from the zenodo entry
- [x] The [gitlab](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db) repo for the database source code needs a DOI to be included here. - TBC when writing paper for JOSS
- [x] What should the format be for citing the source code for the database? - as above
