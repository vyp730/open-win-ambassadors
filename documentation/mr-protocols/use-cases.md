# Use Cases: WIN Open Acquisition Database

>For each case, describe:
1. Who this person is;
2. Their motivation (why they want to use this tool);
3. Goal (what they want to achieve);
4. Where they might first encounter this tool;
5. Their basic interaction to achieve their goal, including access requirements;
6. How we can encourage use of this tool.


## Internal - pilotting

### Who
WIN members, most likely the study researcher.

### Motivation
Keeping track of changes to the protocol (for example seuqences added, sequences modified in volume or parameters) for best practice in reproducibility and prosterity.

### Goal
Maintain an accessible record of what changes were made and why, for own record keeping and communication.

### First encounter
At WIP, or we could introduce as a requirement for pilotting.

### Basic interaction path
1. Told about the repository
2. Receives docs after first pilot session
3. Uploads to repository, with notes
4. Next pilot session => changes made, new docs generated
5. Decide whether a major or minor version change
6. New docs uploaded, with notes indicating change from previous version

### How we can promote uptake
1. Encourage folks who ordinarily pilot (Seb?) to talk about it
2. Encourage radiographers to talk about it
3. Include it in [MRI Scanning Guide](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/Shared%20Documents/MRI_Scanning_Guide.pdf), [Project Code Request Form](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/Shared%20Documents/MRI_Project_Code_Request_Form.docx) or other.


## Internal - sharing internally

### Who
WIN members, primarily users of MRI systems.

### Motivation
Effective sharing complete details of their protocol with another scan user.

### Goal
Provide full and reproducible information about the protocol efficiently, along with details of your decision making process (if used during pilotting).

### First encounter
Asked for a protocol, or asked to share a protocol e.g. with new lab member.

### Basic interaction path
If not already used in pilotting:
1. Asked for a protocol
2. Locates or request docs from radiographers
3. Uploads to protocol repo
4. Check access is set to internal only
5. Shares unique protocol entry url with colleague

### How we can promote uptake
<mark>???</mark>



## Internal - publishing externally

### Who
WIN members, most likely the study researcher or PI.

### Motivation
Include the protocol as supplementary materials in a publication, or sharing with a collaborator.

### Goal
To share the fully reproducible protocol efficiently and with credit

### First encounter
Prompted as WIP or when advertising "papers for publication" in Monday Message

### Basic interaction path
If not already used in (pilotting):
1. Receive prompt at WIP or preparing for publication (or recalls training) that the protocol is a citable research output
2. Requests docs from RadiographersUploads to protocol repository
3. Reserves doi with zenodo
4. Adds reserved doi to protocol entry, along with references to linked outputs (e.g. the manuscript in preparation)
5. Downloads protocol entry with doi and uploads to zenodo
6. Includes call abck to protocols repo in zenodo entry

### How we can promote uptake
1. Ensure mentioned in WIPs
2. Follow up with listings in papers for publication
3. <mark>???</mark>


## External

### Who
MR researchers

### Motivation
Get reproducible (COBIDAS level) methods detail of imaging protocol from a published study or protocol, or a protocol in open development.

### Goal
To efficiently get all details of the protocol for the purposes of review or implementation at their own insitution.

### First encounter
Protocol doi included in published methods, suplementary materials or other public facing project material (e.g. study website).

Browsing repository after learning about it (e.g. from our outreach).

### Basic interaction path
If browsing:
1. Learns of [open.win.ox.ac.uk/protocols/](http://open.win.ox.ac.uk/protocols/) via outreach, e.g. advertising on the WIN website, twitter, talk, or word of mouth.
2. Visits repository, enters search terms or "browse all"
3. Accesses public entries

If following a doi:
1. Finds a doi in a publication
2. Follows doi to zenodo
3. Accesses protocol entry pdf.
4. Follows zenodo entry notes/link to visit the protocols repo to version updates
5. Visits latest version on protocols repo
6. If has questions, follows the contact information in the usage notes
7. If reuses, cites the protocol entry as described in the usage notes


### How we can promote uptake
If browsing:
1. <mark>Ensure flagship and public entries are complete and with top grade documentation - they are our showcase.</mark>
2. <mark>Create a general awareness campaign (e.g. twitter on launch, update website)</mark>
3. <mark>Have awareness of main conferences in the field (e.g. ISMRM, OHBM). Contact WIN and invite them to add their protocol when preparing abstracts.</mark>

If following a doi:
1. Provide boilerplate text to be included on zenodo entries redirecting users to protocols repo. 
