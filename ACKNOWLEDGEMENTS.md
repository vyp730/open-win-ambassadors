# Citing this repository

**Please cite this repository as (authors listed alphabetically):**

Stuart Clare, Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Clare Mackay, Verena Sarrazin, Bernd Taschler (2022). Open WIN Ambassadors: Call notes and documentation drafts. Zenodo. https://doi.org/10.5281/zenodo.7114650

## Author Contributions (credit):

- Conceptualization: Stuart Clare, Cassandra D. Gould van Praag, Clare E. Mackay.
- Data curation: Cassandra D. Gould van Praag.
- Formal analysis: Cassandra D. Gould van Praag.
- Funding acquisition: Stuart Clare, Clare E. Mackay.
- Investigation: Cassandra D. Gould van Praag.
- Methodology: Stuart Clare, Cassandra D. Gould van Praag, Clare E. Mackay.
- Project administration: Stuart Clare, Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Clare E. Mackay, Verena Sarrazin, Bernd Taschler.
- Resources: Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Verena Sarrazin, Bernd Taschler.
- Software: Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Verena Sarrazin, Bernd Taschler.
- Supervision: Stuart Clare, Cassandra D. Gould van Praag, Clare E. Mackay.
- Visualization: Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Verena Sarrazin, Bernd Taschler.
- Writing - original draft: Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Verena Sarrazin, Bernd Taschler.
- Writing - review & editing: Dejan Draschkow, Yingshi Feng, Cassandra Gould van Praag, Verena Sarrazin, Bernd Taschler.

This resource is released under a [CC-BY-4.0 license](LICENSE.md). Copyright University of Oxford, 2021.

# Acknowledgements

We are grateful to the following individuals and resources for inspiration and contributions to this repository.

[Materials and development of the Ambassadors programme](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/ambassadors/) were developed with the support of the [Open Life Sciences (OLS) Mentoring Program](https://openlifesci.org).

Other resources have also been developed following example materials developed by [The Turing Way](https://the-turing-way.netlify.app/welcome).

`The Turing Way Community, Becky Arnold, Louise Bowler, Sarah Gibson, Patricia Herterich, Rosie Higman, … Kirstie Whitaker. (2019, March 25). The Turing Way: A Handbook for Reproducible Data Science. Zenodo http://doi.org/10.5281/zenodo.3233853.`

We are grateful to the [CSCCE Community](https://www.cscce.org) for their support and guidance in all things community management!
