# Open WIN Ambassadors

[![doi](https://zenodo.org/badge/DOI/10.5281/zenodo.7114650.svg)](https://doi.org/10.5281/zenodo.7114650)

## The Problem
WIN is has an ambition of being a leader in open science and is investing heavily in our physical and social infrastructure to achieve this. With a large pool of researchers to reach and support, it is imperative that we have direct links with a variety of users, so we are well equipped to meet their needs. We also benefit from creating opportunities for highly motivated researchers to make significant contributions to these efforts (following the [Mountain of Engagement](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/mountain-of-engagement/) model), while  ensuring they are rewarded and recognised appropriately. We are addressing both of these problems with an Ambassadors Programme.


## The Solution
The Open WIN Ambassadors have applied to participate in a programme to receive training in open research practices, act as a direct link to the wider researcher population, and contribute to the development of documentation and policy relating to open research as it is practiced at WIN. This repository contains materials created by the Ambassadors and notes from our meetings.


## What are we doing?
We are working to achieve the [goals of the Open WIN Ambassadors Programme](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/goals/):
1. Grow the user base of open science infrastructure at WIN.
2. Improve the transparency, reproducibility and integrity of our research at WIN.
3. Build capacity in open and collaborative leadership at WIN.
4. Set the agenda in the implementation of open science at WIN.
5. Shape culture at WIN by demonstrating inclusivity, valuing all contributions, leading with transparency and encouraging non-competitive collaboration.

We aim to achieve these goals through training, advocacy and the creation of materials to support the adoption of open research practices at WIN.

## What do we need?
We need people to participate in the next cohort of the Ambassadors programme! Please consider applying yourself, or suggesting it to your colleagues.

You are also welcome to make direct contributions to this repository or provide your feedback to Cass! Please see our [CONTRIBUTING.md](CONTRIBUTING.md) guide.

## Who are we?
These materials have been created by the [Open WIN Ambassadors](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/meet_ambassadors/): a team of individuals who are significant contributors to the [Open WIN Community](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/community-who/).

## Contact us
Contact the [Open WIN Community Engagement Coordinator](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/community-who/#community-coordinator---cassandra-gould-van-praag-sheher) (cassandra.gouldvanpraag@psych.ox.ac.uk) for discussion or feedback on these materials.

## Acknowledgements and citation
Please see [ACKNOWLEDGEMENTS.md](ACKNOWLEDGEMENTS.md) for a citation guide and full list of influences.
