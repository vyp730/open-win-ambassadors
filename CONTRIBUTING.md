# Contributing

How to contribute to this project

---

We want to ensure that every user and contributor feels welcome and included in this project being conducted by the Open WIN Community. We ask all contributors and community members to follow the [Open WIN Participation Guidelines](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/CODE_OF_CONDUCT/) in all community interactions.

**We hope that this guideline document will make it as easy as possible for you to get involved.**

Please follow these guidelines to make sure your contributions can be easily integrated in the projects. As you start contributing to Open WIN projects, don't forget that your ideas are more important than perfect [merge requests (known as "pull requests" in GitHub)](https://opensource.stackexchange.com/questions/352/what-exactly-is-a-pull-request).

## 1. Contact us
In the first instance, we encourage you [connect with the Open WIN Community](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/). Let us know that you are reading the repository and you would like to contribute! You might like to introduce yourself on the #welcome channel on our [Slack workspace](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/#open-win-slack-)! 👋

## 2. Check what we're working on
Take a look through the [call notes](call-notes) to bring yourself up to speed with our work. If this feels too daunting, revert to (1) and ask to be pointed to the relevant discussion :). 

## 3. Join us in developing this resource!
### Provide feedback
This project is open for feedback and consultation with WIN researchers. We are seeking this feedback to ensure that this programme meets your needs. Let us know what you think by email to cassandra.gouldvanpraag@psych.ox.ac.uk, or on our [Slack workspace](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/#open-win-slack-).

### Apply to the Ambassadors programme
Applications for the next cohort of Open WIN Ambassadors are open! Take a look at the [application procedure](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/ambassadors/application/)

### Submit a merge request or open an issue
If you are comfortable making a merge request in GitLab, we welcome your direct suggestions for edits to the [Ambassadors sections on the Community Pages](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/ambassadors/) or this repository. Alternatively you are very welcome to open an issue on this repository or the community pages, which ever is most relevant for your feedback. 

## 4. Acknowledgements
Any and all contributors to the development of this resource will be listed in the [ACKNOWLEDGEMENTS](ACKNOWLEDGEMENTS.md) for this project. They will also be listed as authors in the zenodo entry for the project digital object identifier when one is generated. 
